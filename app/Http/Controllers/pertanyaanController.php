<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class pertanyaanController extends Controller
{
    public function index(){
        $awal = DB::table('pertanyaan')->get();
        return view('crud.awal', compact('awal'));
    }
    //
    public function create(){
        return view('crud.masuk');
    }
    public function store(Request $request)
    {
        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);
        return redirect('/pertanyaan');
    }

    public function show($id){
        $awal = DB::table('pertanyaan')->where('id', $id)->first();
        return view('crud.show', compact('awal'));
    }
    public function edit($id){
        $awal = DB::table('pertanyaan')->where('id', $id)->first();
        return view('crud.edit', compact('awal'));
    }
}
