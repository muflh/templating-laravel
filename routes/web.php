<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/', function () {
    return view('adminlte.master');
});

Route::get('/data-table', function () {
    return view('adminlte.table.data-tables');
});

Route::get('/pertanyaan',  'pertanyaanController@index');

Route::get('/pertanyaan/create',  'PertanyaanController@create');

Route::post('/pertanyaan',  'pertanyaanController@store');

Route::get('/pertanyaan/{pertanyaan_id}',  'pertanyaanController@show');

Route::get('/pertanyaan/{pertanyaan_id}/edit',  'pertanyaanController@edit');
