
              <div class="card-header">
                <h3 class="card-title">tabel pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <table class="table table-sm">
                  <thead>
                    <tr>
                      <th style="width: 10px">No</th>
                      <th>Judul</th>
                      <th>Isi</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($awal as $key => $awal)
                  <tr>
                  <td> {{ $key + 1}} </td>
                  <td> {{ $awal->judul}} </td>
                  <td> {{ $awal->isi}} </td>
                  <td>
                  <a href="/pertanyaan/{{$awal->id}}" class="btn btn-info btn-sm">lihat</a>
                  <a href="/pertanyaan/{{$awal->id}}" class="btn btn-info btn-sm">edit</a>
                  </td>
                  </tr>
                  @endforeach
                  </tbody>
                 
                </table>
                <a href="/pertanyaan/create" class="btn btn-info btn-sm">tambah data</a>
              </div>
              <!-- /.card-body -->
            