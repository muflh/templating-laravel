
          
         
              <div class="card-header">
                <h3 class="card-title">edit post</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/pertanyaan/{pertanyaan_id}/edit" method= "POST">
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="judul">judul</label>
                    <input type="text" class="form-control" id="judul" name="judul" value="{{old('judul', $awal->judul}}" placeholder="Masukkan Judul">
                  </div>
                  <div class="form-group">
                    <label for="isi">isi</label>
                    <input type="text" class="form-control" id="isi" name="isi" value="{{old('isi', $awal->isi}}" placeholder="Masukkan isi">
                  </div>
                  
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
